<?php include 'utils.php';

writePageHeader("Search Page");

echo "<body>";


writeMenu();
echo "<br />";

if (isset($_POST["REPORT"]))
{
        $report = $_POST["REPORT"];

        $listname = $_POST["LISTNAME"];
        if (strcmp($report,"ListCompByProd")==0 && !isset($_POST["LISTNAME"]))
        {

                echo "<form action='search.php' method='post'>";

                echo "Please select a PRODUCT to see the occurences per Company by PRODUCT, report may take up to 15 seconds.<br />";

                echo "<select name='PRODUCT' >";


                callProc($hostname, $username, $password, $database, "GetDistinctProd","DDL", "GetDistinctProd");

                echo "</select>";
                echo "<input name='REPORT' type='hidden' value='" . $report . "' />";
                echo "<input name='LISTNAME' type='hidden' value='SUBMIT' />";
                echo "<input name='submit' type='submit' text='submit'>";

                echo "</form>";

        }
        elseif(strcmp($report,"ListCompByProd")==0 && isset($_POST["LISTNAME"]))
        {
                callProc($hostname, $username, $password, $database, "ListCompByProd('" . $_POST["PRODUCT"] . "')","", "ListCompByProd");
        }
        elseif(strcmp($report,"ListCompByComp")==0 && !isset($_POST["LISTNAME"]))
        {
                echo "<form action='search.php' method='post'>";
                echo "Please select a COMPANY to see the occurences by Company, report may take up to 15 seconds.<br />";
                echo "<select name='COMPANY' >";
                callProc($hostname, $username, $password, $database,"GetDistinctComp","DDL", "GetDistinctComp");
                echo "</select>";
                echo "<input name='REPORT' type='hidden' value='" .$report . "' />";
                echo "<input name='LISTNAME' type='hidden' value='SUBMIT' />";
                echo "<input name='submit' type='submit' text='submit'>";
                echo "</form>";
        }
        elseif(strcmp($report,"ListCompByComp")==0 && isset($_POST["LISTNAME"]))
        {
                callProc($hostname, $username, $password, $database, "ListCompByComp('" . $_POST["COMPANY"] . "')","","ListCompByComp");
        }
        elseif(strcmp($report, "CompByCompByProdWInc")==0 && !isset($_POST["LISTNAME"]))
        {
                echo "<form action='search.php' method='post'>";
                echo "Please select a COMPANY to see occurences by Company with Income by zip code, report may take up to 15 seconds.<br />";
                echo "<select name='COMPANY' >";
                callProc($hostname, $username, $password, $database, "GetDistinctComp","DDL","GetDistinctComp");
                echo "</select>";
                echo "<input name='REPORT' type='hidden' value='" .$report . "' />";
                echo "<input name='LISTNAME' type='hidden' value='SUBMIT' />";
                echo "<input name='submit' type='submit' text='submit'>";
                echo "</form>";
        }
        elseif(strcmp($report,"CompByCompByProdWInc")==0 && isset($_POST["LISTNAME"]))
        {
                callProc($hostname, $username, $password, $database, "CompByCompByProdWInc('" . $_POST["COMPANY"] . "')","","CompByCompByProdWInc");
        }
        elseif(strcmp($report,"CompByCompWInc")==0 && !isset($_POST["LISTNAME"]))
        {
                echo "<form action='search.php' method='post'>";
                echo "Please select a COMPANY to see occurences by Company with Income, report may take up to 15 seconds.<br />";
                echo "<select name='COMPANY'>";
                callProc($hostname, $username, $password, $database, "GetDistinctComp","DDL", "GetDistinctComp");
                echo "</select>";
                echo "<input name='REPORT' type='hidden' value='" . $report ."' />";
                echo "<input name='LISTNAME' type='hidden' value='SUBMIT' />";
                echo "<input name='submit' type='submit' text='submit'>";
                echo "</form>";
        }
        elseif(strcmp($report,"CompByCompWInc")==0 && isset($_POST["LISTNAME"]))
        {
                callProc($hostname, $username, $password, $database, "CompByCompWInc('" . $_POST["COMPANY"] . "')","","CompByCompWInc");
        }
        elseif(strcmp($report, "CompByCompByProdAndStateWInc")==0 && !isset($_POST["LISTNAME"]))
        {
                echo "<form action='search.php' method='post'>";
                echo "Please select a COMPANY to see occurences by Company and State with Income, report may take up to 15 seconds.<br />";
                echo "<select name='COMPANY'>";
                callProc($hostname, $username, $password, $database, "GetDistinctComp","DDL","GetDistinctComp");
                echo "</select>";
                echo "<br />";
                echo "Please select a State";
                echo "<br />";
                echo "<select name='STATE'>";
                CallProc($hostname, $username, $password, $database, "GetDistinctState","DDL","GetDistinctState");
                echo "</select>";
                echo "<input name='REPORT' type='hidden' value='" . $report . "' />";
                echo "<input name='LISTNAME' type='hidden' value='SUBMIT' />";
                echo "<input name='submit' type='submit' text='submit'>";
                echo "</form>";
        }
        elseif(strcmp($report,"CompByCompByProdAndStateWInc")==0 && isset($_POST["LISTNAME"]))
        {
                callProc($hostname, $username, $password, $database, "CompByCompByProdAndStateWInc('" . $_POST["COMPANY"] ."', '" . $_POST["STATE"] . "')","","CompByCompByProdAndStateWInc");
        }
}
else
{
        writePageForm("BLANK");
}
echo "</body>";

writePageFooter();


//handle writing the form inputs
function writePageForm($menu)
{
        echo "<form action='search.php' method='post'>";
        if ((strcmp($menu, "BLANK")==0) )
        {
                echo "Please select a report to run";
                echo "<br />";
                echo "<select name='REPORT' >";
                echo "<option value='ListCompByProd'>List Complaints By Product</option>";
                echo "<option value='ListCompByComp'>List Complaints By Company</option>";
                echo "<option value='CompByCompByProdWInc'>List Complaints By Company Aggregated By Location/Product w Income</option>";
                echo "<option value='CompByCompWInc'>List Complaints By Company w Income</option>";
                echo "<option value='CompByCompByProdAndStateWInc'>List Complaints By Company And State w Income</option>";
                echo "</select>";
        }

        echo "<input name='SUBMIT' type='SUBMIT' TEXT='SUBMIT' />";

        echo "</form>";
}
?>
                                                        