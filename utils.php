<?php
//define global variables
$hostname = "localhost";
$username = "webuser";
$password = "mytestpass";
$database = "cfpb";




//handle DB connections, db selections, and proc calls.  also handles how to render the output depending if it is a DDL vs Dataset.
function callProc($host, $user, $pw, $db, $procname, $type, $name)
{

        $mysqli = new mysqli($host, $user, $pw);
        if(!$mysqli) die ('Could not connect to server: ' . mysql_error());

        mysqli_select_db($mysqli, $db);
        if(!$mysqli) die ('Could not connect to DB: ' . mysql_error());

        if (strcmp($procname,"GetDistinctProd")==0)
        {
                $result = $mysqli->query("Call GetDistinctProd()");
        }
        elseif (strpos($procname,"ListCompByProd")!==false)
        {
                $result = $mysqli->query("Call " . $procname);
        }
        elseif(strcmp($procname,"GetDistinctComp")==0)
        {
                $result = $mysqli->query("Call GetDistinctComp()");
        }
        elseif(strcmp($procname,"ListCompByComp")!==false)
        {
                $result = $mysqli->query("Call " . $procname);
        }
        elseif(strcmp($procname,"CompByCompByProdWInc")!==false)
        {
                $result = $mysqli->query("Call " . $procname);
        }
        elseif(strcmp($procname,"CompByCompWInc")!== false)
        {
                $result = $mysqli->query("Call " . $procname);
        }
        elseif(strcmp($procname,"CompByCompByProdAndStateWInc")!==false)
        {
                $result = $mysqli->query("Call " . $procname);
        }
        elseif(strcmp($procname,"GetDistinctState")==0)
        {
                $result = $mysqli->query("Call GetDistinctState()");
        }
        else
        {
                $result = $mysqli->query("Call error()");
        }
        if(!$result) die ("Call failed: (" . $mysqli->errno . ") " . $mysqli->error);


        if(strcmp($type, "DDL")==0)
        {
                writeDDLOutput($result, $type, $name);
        }
        else
        {
                writeDBOutput($result, $type, $name);
        }
}


//write page header tags
function writePageHeader($title)
{
        echo "<html><head><title>" . $title . "</title></head>";
}

//write page closer tags
function writePageFooter()
{
        echo "</html>";
}

//write the top page menue
function writeMenu()
{
        echo "<table  style='border-width:thin;border-style:inset;cell-padding:none;cell-spacing:none;'>";
        echo "<tr  style='min-width:600px' style='border-width:thin;border-style:solid;'>";
        echo "<td style='border-width:thin;border-style:solid;'>Reports for Consumer Complaints<td style='border-width:thin;border-style:solid;'><a href='search.php'>Reset</a></td>";
        echo "</tr>";
        echo "</table>";
}

//write the DDL selection options
function writeDDLOutput($rezset, $purpose, $title)
{
echo $title;
        if($rezset->num_rows >0)
        {
                while($row = mysqli_fetch_array($rezset,MYSQLI_ASSOC))
                {

                        echo "<option value='";
                        if (strcmp($title,"GetDistinctProd")==0)
                        {
                                echo $row["product"] . "'>" . $row["product"] ;
                        }
                        elseif(strcmp($title,"GetDistinctComp")==0)
                        {
                                echo $row["company"] . "'>" . $row["company"];
                        }
                        elseif(strcmp($title,"GetDistinctState")==0)
                        {
                                echo $row["state"] . "'>" . $row["state"];
                        }
                        echo "</option>";
                }
        }
        else
        {
                echo "<option value='ERR'>No rows returned!</option>";
        }
}


//write the results from the query
function writeDBOutput($rezset, $purpose, $title)
{
        echo "<table style='border-width:thin;border-style:inset;cell-padding:none;cell-spacing:none;'>";
        if($rezset->num_rows >0)
        {
                writeDBHeader($title);

                while($row = mysqli_fetch_array($rezset,MYSQLI_ASSOC))
                {
                        echo "<tr style='min-width:600px' style='border-width:thin;border-style:solid;'>";

                        if(strcmp($title,"ListCompByProd")==0)
                        {
                                echo  "<td style='border-width:thin;border-style:solid;'>" . $row["PRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["COMPANY"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["cnt"] . "</td>";
                        }
                        elseif(strcmp($title,"ListCompByComp")==0)
                        {
                                echo "<td style='border-width:thin;border-style:solid;'>" . $row["PRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["SUBPRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["ISSUE"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["STATE"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["NumbOfComplaints"] . "</td>";
                        }
                        elseif(strcmp($title,"CompByCompByProdWInc")==0)
                        {
                                echo "<td style='border-width:thin;border-style:solid;'>" . $row["COMPANY"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["PRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["SUBPRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["NOINC"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["1KTO10K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["10KTO15K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["15KTO25K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["25KTO35K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["35KTO50K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["50KTO65K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["65KTO75K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["75KPLUS"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["COMPLAINTCNT"] . "</td>";
                        }
                        elseif(strcmp($title,"CompByCompWInc")==0)
                        {
                                 echo "<td style='border-width:thin;border-style:solid;'>" . $row["PRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["SUBPRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["ISSUE"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["SUBISSUE"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["STATE"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["ZIPCODE"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["NOINC"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["1KTO10K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["10KTO15K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["15KTO25K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["25KTO35K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["35KTO50K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["50KTO65K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["65KTO75K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["75KPLUS"] . "</td>";
                        }
                        elseif(strcmp($title,"CompByCompByProdAndStateWInc")==0)
                        {
                                echo "<td style='border-width:thin;border-style:solid;'>" . $row["COMPANY"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["PRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["SUBPRODUCT"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["NOINC"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["1KTO10K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["10KTO15K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["15KTO25K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["25KTO35K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["35KTO50K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["50KTO65K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["65KTO75K"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["75KPLUS"] . "</td><td style='border-width:thin;border-style:solid;'>" . $row["COMPLAINTCNT"] . "</td>";
                        }

                         echo "</tr>";
                }

        }
        else
        {
                echo "No rows returned!";
        }
        echo "</table>";

}



//write the table heders for the data
function writeDBHeader($setname)
{
        if(strcmp($setname,"ListCompByProd")==0)
        {
                echo "<tr style='min-width:600px' style='border-width:thin;border-style:solid;'><td style='border-width:thin;border-style:solid;'>PRODUCT</td><td style='border-width:thin;border-style:solid;'>COMPANY</td><td style='border-width:thin;border-style:solid;'>COUNT</td></tr>";
        }
        elseif(strcmp($setname,"ListCompByComp")==0)
        {
                echo "<tr style='min-width:600px' style='border-width:thin;border-style:solid;'><td style='border-width:thin;border-style:solid;'>PRODUCT</td><td style='border-width:thin;border-style:solid;'>SUBPRODUCT</td><td style='border-width:thin;border-style:solid;'>ISSUE</td><td style='border-width:thin;border-style:solid;'>STATE</td><td style='border-width:thin;border-style:solid;'>Number of Complaints</td></tr>";
        }
        elseif(strcmp($setname,"CompByCompByProdWInc")==0)
        {
                echo "<tr style='min-width:600px' style='border-width:thin;border-style:solid;'><td style='border-width:thin;border-style:solid;'>COMPANY</td><td style='border-width:thin;border-style:solid;'>PRODUCT</td><td style='border-width:thin;border-style:solid;'>SUBPRODUCT</td><td style='border-width:thin;border-style:solid;'># w No Inc.</td><td style='border-width:thin;border-style:solid;'># w Inc 1K to 10K</td><td style='border-width:thin;border-style:solid;'># w Inc 10K to 15K</td><td style='border-width:thin;border-style:solid;'># w Inc 15K to 25K</td><td style='border-width:thin;border-style:solid;'># w Inc 25K to 35K</td><td style='border-width:thin;border-style:solid;'># w Inc 35K to 50K</td><td style='border-width:thin;border-style:solid;'># w Inc 50K to 65K</td><td style='border-width:thin;border-style:solid;'># w Inc 65K to 75K</td><td style='border-width:thin;border-style:solid;'># w Inc 75K+</td><td style='border-width:thin;border-style:solid;'>Number of Complaints</td></tr>";
        }
        elseif(strcmp($setname,"CompByCompWInc")==0)
        {
                echo "<tr style='min-width:600px' style='border-width:thin;border-style:solid;'><td style='border-width:thin;border-style:solid;'>PRODUCT</td><td style='border-width:thin;border-style:solid;'>SUBPRODUCT</td><td style='border-width:thin;border-style:solid;'>ISSUE</td><td style='border-width:thin;border-style:solid;'>SUBISSUE</td><td style='border-width:thin;border-style:solid;'>STATE</td><td style='border-width:thin;border-style:solid;'>ZIPCODE</td><td style='border-width:thin;border-style:solid;'># w No Inc.</td><td style='border-width:thin;border-style:solid;'># w Inc 1K to 10K</td><td style='border-width:thin;border-style:solid;'># w Inc 10K to 15K</td><td style='border-width:thin;border-style:solid;'># w Inc 15K to 25K</td><td style='border-width:thin;border-style:solid;'># w Inc 25K to 35K</td><td style='border-width:thin;border-style:solid;'># w Inc 35K to 50K</td><td style='border-width:thin;border-style:solid;'># w Inc 50K to 65K</td><td style='border-width:thin;border-style:solid;'># w Inc 65K to 75K</td><td style='border-width:thin;border-style:solid;'># w Inc 75K+</td></tr>";
        }
        elseif(strcmp($setname,"CompByCompByProdAndStateWInc")==0)
        {
                echo "<tr style='min-width:600px' style='border-width:thin;border-style:solid;'><td style='border-width:thin;border-style:solid;'>COMPANY</td><td style='border-width:thin;border-style:solid;'>PRODUCT</td><td style='border-width:thin;border-style:solid;'>SUBPRODUCT</td><td style='border-width:thin;border-style:solid;'># w No Inc.</td><td style='border-width:thin;border-style:solid;'># w Inc 1K to 10K</td><td style='border-width:thin;border-style:solid;'># w Inc 10K to 15K</td><td style='border-width:thin;border-style:solid;'># w Inc 15K to 25K</td><td style='border-width:thin;border-style:solid;'># w Inc 25K to 35K</td><td style='border-width:thin;border-style:solid;'># w Inc 35K to 50K</td><td style='border-width:thin;border-style:solid;'># w Inc 50K to 65K</td><td style='border-width:thin;border-style:solid;'># w Inc 65K to 75K</td><td style='border-width:thin;border-style:solid;'># w Inc 75K+</td><td style='border-width:thin;border-style:solid;'>Number of Complaints</td></tr>";
        }

}

?>